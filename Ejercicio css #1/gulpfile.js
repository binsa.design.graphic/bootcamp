const browserSync = require('browser-sync').create();
const gulp = require('gulp');
const sass = require('gulp-sass');
var rutaCSS = 'deploy/css/'; // ruta del CSS final
var rutaJS = 'deploy/js/'; // ruta del archivo JS final
var rutaIMG = 'deploy/img/'; //carpeta donde compila las imágenes
var nameJS = 'mainscript.js'; //nombre final de los archivos concatenados

var gulp = require('gulp'),
  plumber = require('gulp-plumber'),
  rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin'),
  cache = require('gulp-cache');
var minifycss = require('gulp-minify-css');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('browser-sync', function () {
  browserSync.init({
    server: {
      baseDir: "./deploy/"
    }
  });
});

// gulp.task('images', function(){
//   gulp.src('sources/images/*')
//     .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
//     .pipe(gulp.dest(rutaIMG));
// });

gulp.task('sass', () => {
  return gulp.src([
    'node_modules/bootstrap/scss/bootstrap.scss',
    'src/scss/*.scss'
  ])
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(gulp.dest('deploy/css'));
});

gulp.task('js', () => {
  return gulp.src([
    'node_modules/bootstrap/deploy/bootstrap.min.js',
    'node_modules/jquery/deploy/jquery.min.js',
    'node_modules/popper.js/deploy/umd/popper.min.js'
  ])
    .pipe(gulp.dest('deploy/js'))
    .pipe(browserSync.stream());
});

gulp.task('font-awesome', () => {
  return gulp.src('node_modules/font-awesome/css/font-awesome.min.css')
    .pipe(gulp.dest('deploy/css'));
});

gulp.task('fonts', () => {
  return gulp.src('node_modules/font-awesome/fonts/*')
    .pipe(gulp.dest('deploy/fonts'));
});


gulp.task('images', () =>
  gulp.src('sources/images/*/*')
    .pipe(imagemin())
    .pipe(gulp.dest(rutaIMG))
);

gulp.task('styles', function () {
  gulp.src(['sources/sass/main.scss'])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer('last 2 versions'))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(rutaCSS))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss({ keepSpecialComments: 1, processImport: false }))
    .pipe(gulp.dest(rutaCSS))
});

gulp.task('scripts', function () {
  // return gulp.src('sources/js/*.js')
  return gulp.src([
    'sources/js/utilidades.js',
    'sources/js/script.js',
    'sources/js/front.js'
  ])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }))
    .pipe(concat(nameJS))
    .pipe(gulp.dest(rutaJS))
    .pipe(rename({ suffix: '.min' }))
  //.pipe(uglify())
  //.pipe(gulp.dest(rutaJS))
});

gulp.task('watch', function () {
  //gulp.watch('jade/*.jade',['templates']);
  //gulp.watch('jade/includes/*.jade',['templates']);
  // ↓↓↓ Sass watch
  gulp.watch('sources/sass/**/*.scss', ['styles']);
  gulp.watch('sources/js/*.js', ['scripts']);
  // gulp.watch('sources/images/*/*', ['images']);
  //gulp.watch('js/*.js', ['compress']);
  // ↑↑↑ Sass watch
});

//gulp.task('default', function(){
//  gulp.watch("sources/sass/**/*.scss", ['styles']);
//  gulp.watch("sources/js/**/*.js", ['scripts']);
//});

// tareas default
gulp.task('default', ['styles', 'scripts', 'watch', 'browser-sync']);
